// @flow

import React, { memo } from "react"
import SidebarBoxContainer from "../SidebarBoxContainer"
import DescriptionIcon from "@material-ui/icons/Description"
import { styled } from "@material-ui/core/styles"
import { grey } from "@material-ui/core/colors"
import Markdown from "react-markdown"
import {
  Typography,
  CardContent,
  Card,
  Chip,
} from '@material-ui/core/';
import LinearProgressWithLabel from '../../shared/customComponents/LinearProgressWithLabel';
import MoreSharpIcon from '@material-ui/icons/MoreSharp';

const MarkdownContainer = styled("div")({
  paddingLeft: 16,
  paddingRight: 16,
  fontSize: 12,
  "& h1": { fontSize: 18 },
  "& h2": { fontSize: 14 },
  "& h3": { fontSize: 12 },
  "& h4": { fontSize: 12 },
  "& h5": { fontSize: 12 },
  "& h6": { fontSize: 12 },
  "& p": { fontSize: 12 },
  "& a": {},
  "& img": { width: "100%" },
})


export const TaskDescriptionSidebarBox = ({ asset }) => {
         return (
           <SidebarBoxContainer
             title="Asset Information"
             icon={
               <DescriptionIcon
                 style={{ color: grey[700] }}
               />
             }
             expandedByDefault={
               asset && asset !== '' ? false : true
             }
           >
             {/* <MarkdownContainer>
        <Markdown source={description} />
      </MarkdownContainer> */}

             {asset && (
               <div style={{ padding: '10px' }}>
                 <div>
                   {/* <Typography variant="h6">
                     {' '}
                    //  Asset Tags
                   </Typography> */}
                   <hr />
                   <Chip
                     icon={<MoreSharpIcon />}
                     label={asset.id}
                     color="primary"
                   />
                 </div>
                 <br />

                 <div>
                   <Typography variant="h6">
                     {' '}
                     Coordinates
                   </Typography>
                   {/* <hr /> */}
                   <Typography>
                     {asset.geographicCoordinates[0]},{' '}
                     {asset.geographicCoordinates[1]}
                   </Typography>
                 </div>

                 <br />
                 {/* <div>
                   <Typography variant="h6">
                     {' '}
                     Components
                   </Typography>
                   <hr />
                 </div> */}
               </div>
             )}
           </SidebarBoxContainer>
         );
       };

export default memo(TaskDescriptionSidebarBox)

